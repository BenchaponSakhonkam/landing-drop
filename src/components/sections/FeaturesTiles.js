import React from 'react';
import classNames from 'classnames';
import { SectionTilesProps } from '../../utils/SectionProps';
import SectionHeader from './partials/SectionHeader';
import Image from '../elements/Image';

const propTypes = {
  ...SectionTilesProps.types
}

const defaultProps = {
  ...SectionTilesProps.defaults
}
const FeaturesTiles = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  pushLeft,
  ...props
}) => {

  const outerClasses = classNames(
    'features-tiles section',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'features-tiles-inner section-inner pt-0',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );

  const tilesClasses = classNames(
    'tiles-wrap center-content',
    pushLeft && 'push-left'
  );

  const sectionHeader = {
    title: 'Drop Thailand',
    paragraph: 'ข้อมูลเบื้องต้นของเรา'
  };

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader data={sectionHeader} className="center-content" />
          <div className={tilesClasses}>

            <div className="tiles-item reveal-from-bottom" data-reveal-delay="200">
              <div className="tiles-item-inner">
                <div className="features-tiles-item-header">
                  <div className="features-tiles-item-image mb-16">
                    <Image
                      src={require('./../../assets/images/user.jpg')}
                      alt="user"
                      width={350}
                      height={350} />
                  </div>
                </div>
                <div className="features-tiles-item-content ">
                  <h3 className="mt-5 mb-8">
                    ผู้ใช้งานทั้งหมด
                    </h3>
                    <h4 className="mt-0 mb-8">
                    120 คน
                    </h4>
                </div>
              </div>
            </div>

            <div className="tiles-item reveal-from-bottom">
              <div className="tiles-item-inner">
                <div className="features-tiles-item-header">
                  <div className="features-tiles-item-image mb-16">
                    <Image
                      src={require('./../../assets/images/order.jpg')}
                      alt="order"
                      width={350}
                      height={350} />
                  </div>
                </div>
                <div className="features-tiles-item-content">
                  <h3 className="mt-5 mb-8">
                    จำนวนครั้งที่ใช้งาน
                    </h3>
                    <h4 className="mt-0 mb-8">
                    220 ครั้ง
                    </h4>
                </div>
              </div>
            </div>

            <div className="tiles-item reveal-from-bottom" data-reveal-delay="200">
              <div className="tiles-item-inner">
                <div className="features-tiles-item-header">
                  <div className="features-tiles-item-image mb-16">
                    <Image
                      src={require('./../../assets/images/money.jpg')}
                      alt="Features tile icon 04"
                      width={350}
                      height={350} />
                  </div>
                </div>
                <div className="features-tiles-item-content">
                  <h3 className="mt-5 mb-8">
                    รายได้ที่สร้างในร้าน
                    </h3>
                    <h4 className="mt-0 mb-8">
                    ทั้งหมด 68,095 บาท
                    </h4>
                </div>
              </div>
            </div>

           

          </div>
        </div>
      </div>
    </section>
  );
}

FeaturesTiles.propTypes = propTypes;
FeaturesTiles.defaultProps = defaultProps;

export default FeaturesTiles;