import React from "react";
import classNames from "classnames";
import { SectionSplitProps } from "../../utils/SectionProps";
import SectionHeader from "./partials/SectionHeader";
import Image from "../elements/Image";
import Button from "../elements/Button";
import ButtonGroup from "../elements/ButtonGroup";
const propTypes = {
  ...SectionSplitProps.types,
};

const defaultProps = {
  ...SectionSplitProps.defaults,
};

const FeaturesSplit = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  invertMobile,
  invertDesktop,
  alignTop,
  imageFill,
  ...props
}) => {
  const outerClasses = classNames(
    "features-split section",
    topOuterDivider && "has-top-divider",
    bottomOuterDivider && "has-bottom-divider",
    hasBgColor && "has-bg-color",
    invertColor && "invert-color",
    className
  );

  const innerClasses = classNames(
    "features-split-inner section-inner",
    topDivider && "has-top-divider",
    bottomDivider && "has-bottom-divider"
  );

  const splitClasses = classNames(
    "split-wrap",
    invertMobile && "invert-mobile",
    invertDesktop && "invert-desktop",
    alignTop && "align-top"
  );

  const sectionHeader = {
    title: "ทำไมต้องเรียก DropMan",
    paragraph:
      "บอกลาความวุ่นวายในชีวิต ทำให้คุณมีความสุขในการซักอบได้ทุกวัน",
  };

  return (
    <section {...props} className={outerClasses}>
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader data={sectionHeader} className="center-content" />
          <div className={splitClasses}>
            <div className="split-item">
              <div
                className="split-item-content center-content-mobile reveal-from-left"
                data-reveal-container=".split-item"
              >
                <div className="text-s text-color-primary fw-600 tt-u mb-8">
                 เรียก DropMan สิ
                </div>
                <h3 className="mt-0 mb-12">รับไปร้านซักอบที่คุณชื่นชอบ และส่งถึงหน้าบ้านคุณ</h3>
                <p className="m-0 mb-12">
                  ไม่ต้องเสียเวลาไปนั่งรอที่ร้าน รอคิวกว่าเครื่องจะว่าง ขับรถลุยอากาศที่แปรปรวนตลอดเวลา
                </p>
              </div>
             
              <div
                className={classNames(
                  "split-item-image center-content-mobile reveal-from-bottom",
                  imageFill && "split-item-image-fill"
                )}
                data-reveal-container=".split-item"
              >
                <Image
                  src={require("./../../assets/images/landing2.jpg")}
                  alt="Features split 01"
                  width={500}
                  height={396}
                />
              </div>
            </div>
           
            <div className="split-item">
              <div
                className="split-item-content center-content-mobile reveal-from-right"
                data-reveal-container=".split-item"
              >
                <div className="text-s text-color-primary fw-600 tt-u mb-8">
                  Partner Wash&Dry.
                </div>
                <h3 className="mt-0 mb-12">ร้านซักอบที่อยากเป็นPartner</h3>
                <p className="m-0 mb-12">
                  เพิ่มฐานลูกค้า รายได้และออเดอร์ของร้าน ให้มากขึ้น
                </p>
                <div className="reveal-from-bottom" data-reveal-delay="200">
                  <ButtonGroup>
                    <Button
                      tag="a"
                      color="primary"
                      wideMobile
                      href="https://github.com/cruip/open-react-template/"
                    >
                      สมัครเป็น Partner กับเรา
                    </Button>
                  </ButtonGroup>
                </div>
              </div>
              <div
                className={classNames(
                  "split-item-image center-content-mobile reveal-from-bottom",
                  imageFill && "split-item-image-fill"
                )}
                data-reveal-container=".split-item"
              >
                <Image
                  src={require("./../../assets/images/landing3.jpg")}
                  alt="Features split 02"
                  width={528}
                  height={396}
                />
              </div>
            </div>

            <div className="split-item">
              <div
                className="split-item-content center-content-mobile reveal-from-left"
                data-reveal-container=".split-item"
              >
              <div className="text-s text-color-primary fw-600 tt-u mb-8">
                  Partner DropMan.
                </div>
                <h3 className="mt-0 mb-12">DropMan Partner หารายได้กับพวกเรา</h3>
                <p className="m-0 mb-12">
                  รับจำนวนคนจำกัด รายได้ 800 บาท / วัน 
                </p>
                <div className="reveal-from-bottom" data-reveal-delay="200">
                  <ButtonGroup>
                    <Button
                      tag="a"
                      color="primary"
                      wideMobile
                      href="https://github.com/cruip/open-react-template/"
                    >
                      เป็น DropMan กับเรา
                    </Button>
                  </ButtonGroup>
                </div>
                </div>
              <div
                className={classNames(
                  "split-item-image center-content-mobile reveal-from-bottom",
                  imageFill && "split-item-image-fill"
                )}
                data-reveal-container=".split-item"
              >
                <Image
                  src={require("./../../assets/images/landing4.png")}
                  alt="Features split 03"
                  width={528}
                  height={396}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

FeaturesSplit.propTypes = propTypes;
FeaturesSplit.defaultProps = defaultProps;

export default FeaturesSplit;
