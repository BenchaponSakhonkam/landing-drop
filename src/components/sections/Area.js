import React from 'react';
import classNames from 'classnames';
import { SectionTilesProps } from '../../utils/SectionProps';
import SectionHeader from './partials/SectionHeader';
import Image from "../elements/Image";

const propTypes = {
  ...SectionTilesProps.types
}

const defaultProps = {
  ...SectionTilesProps.defaults
}

const Testimonial = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  pushLeft,
  ...props
}) => {

  const outerClasses = classNames(
    'testimonial section',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'testimonial-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );

  const tilesClasses = classNames(
    'tiles-wrap',
    pushLeft && 'push-left'
  );

  const sectionHeader = {
    title: 'พื้นที่เปิดให้บริการ',
    paragraph: 'ผู้ใช้สามารถดูพื้นที่จากตรงนี้ได้เลย'
  };

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container">
        <div className={innerClasses}>
          <SectionHeader data={sectionHeader} className="center-content" />
          <div className={tilesClasses}>

            <div className="tiles-item reveal-from-right" data-reveal-delay="200">
            <Image
                    src={require("./../../assets/images/area1.jpg")}
                    alt="nakhonpathom"
                   
                  />
                   <div className="testimonial-item-footer text-s mt-32 mb-0 has-top-divider">
                  <span className="testimonial-item-name text-color-high">นครปฐม</span>
                  <span className="text-color-low"> / </span>
                  <span className="testimonial-item-link">
                    <a href="#0">เปิดให้บริการแล้ว</a>
                  </span>
                </div>
            </div>

            <div className="tiles-item reveal-from-bottom">
                
                <Image
                    src={require("./../../assets/images/area2.jpg")}
                    alt="bankok"
                    
                  />
                <div className="testimonial-item-footer text-s mt-32 mb-0 has-top-divider">
                  <span className="testimonial-item-name text-color-high">กรุงเทพมหานคร</span>
                  <span className="text-color-low"> / </span>
                  <span className="testimonial-item-link">
                    <a href="#0">เร็วๆนี้</a>
                  </span>
                </div>
              </div>
          
            <div className="tiles-item reveal-from-left" data-reveal-delay="200">
                <Image
                    src={require("./../../assets/images/area2.jpg")}
                    alt="bankok"
                    
                  />
                <div className="testimonial-item-footer text-s mt-32 mb-0 has-top-divider">
                  <span className="testimonial-item-name text-color-high">สมุทรสาคร</span>
                  <span className="text-color-low"> / </span>
                  <span className="testimonial-item-link">
                    <a href="#0">เร็วๆนี้</a>
                  </span>
                </div>
              </div>

          </div>
        </div>
      </div>
    </section>
  );
}

Testimonial.propTypes = propTypes;
Testimonial.defaultProps = defaultProps;

export default Testimonial;